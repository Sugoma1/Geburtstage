const App = {
    data(){
        return {
            kontakte: [
                {id: 0, name: 'Anna',geburtsdatum: '20.12.1968'}, 
                {id: 1, name: 'Berta',geburtsdatum: '13.3.1973'}, 
                {id: 2, name: 'Carla',geburtsdatum: '7.7.1977'}
            ],
            readVisible: true,
            createVisible: false,
            editVisible: false,
            deleteVisible: false,
            newPersonName: "",
            newPersonBirthdate: "2000-01-01",
            editPersonName: "",
            editPersonBirthdate:"",
            editPersonFinal: "",
            pushDeletePerson: "",
            pushEditPerson: ""
            
        }
    },
    methods:{
        buttonNewPerson(){
            this.newPersonName = "";
            this.newPersonBirthdate = "2000-01-01"
            this.readVisible = false;
            this.createVisible = true;
            //this.newPerson.id = this.kontakte[-1] + 1
        },

        buttonSaveNewPerson(){
            // Neuen Datensatz speichern
            const newPerson = {
                name: this.newPersonName, geburtsdatum: this.newPersonBirthdate
            }
            this.kontakte.push(newPerson)


            // Zum Statfenster zurück
            this.createVisible = false;
            this.readVisible = true;
            //console.log("lol")
        },

        cancelNewPersonClick(){
            this.readVisible = true;
            this.createVisible = false;
            //console.log("lol")

        },

        editNewPersonClick(kontakt){
            this.readVisible = false;
            this.editVisible = true;
            //console.log(kontakt)
            this.pushEditPerson = kontakt;
            console.log(this.pushEditPerson)
        },

        cancelEditedPerson(){
            this.readVisible = true;
            this.editVisible = false;
        },

        buttonSaveEditedPerson(){
            // console.log(this.pushEditPerson)
            // console.log(this.editPersonBirthdate)
            this.editVisible = false;
            this.readVisible = true;
            let index = this.kontakte.indexOf(this.pushEditPerson)
            if(this.kontakte.includes(this.pushEditPerson)){
                // this.editPersonFinal = this.editPersonName + this.editPersonBirthdate
                // this.kontakte.name = this.editPersonName
                // this.kontakte.geburtsdatum = this.editPersonBirthdate;
                this.kontakte[index].name = this.editPersonName;
                this.kontakte[index].geburtsdatum = this.editPersonBirthdate;
                // this.kontakte[index] = this.editPersonFinal
                // localStorage.setItem(this.kontakte[index].name,  this.editPersonName)
                // localStorage.setItem(this.kontakte[index].geburtsdatum,  this.editPersonBirthdate)

            }
            // console.log(this.kontakte.indexOf(this.pushEditPerson))
            // console.log(this.kontakte.geburtsdatum)
        },

        deletePersonClick(kontakt){
            this.readVisible = false;
            this.deleteVisible = true;
            this.pushDeletePerson = kontakt
            //console.log(this.pushDeletePerson)
            this.kontakt.charAt()
        },

        cancelDeletion(){
            this.readVisible = true;
            this.deleteVisible = false;
        },

        deleteEnable(){
            //this.kontakte.slice(kontakt)
            this.readVisible = true;
            this.deleteVisible = false;
            if(this.kontakte.includes(this.pushDeletePerson)){
                //console.log(this.kontakte.indexOf(this.pushDeletePerson))
                this.kontakte.splice((this.kontakte.indexOf(this.pushDeletePerson)), 1)
                
            }
        }
    }
};
Vue.createApp(App).mount('#app')